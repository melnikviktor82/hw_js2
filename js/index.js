"use strict";

// ## Теоретичні питання

// 1. Які існують типи даних у Javascript?

// Примітивні(прості): числа, строки, Boolean, null, undefined, NaN та об'єктні: об'єкт, массив, функція.

// 2. У чому різниця між == і ===?

// == нестроге порівняння, === строге порівняння(не проводить перетворення типів)

// 3. Що таке оператор?

// Оператор це команда, що виконує якусь дію з операндами(порівнює, змінює, виводить, тощо)

let userName = prompt("What is your name?");

while (userName === null || userName === "") {
  userName = prompt("What is your name?");
}

let userAge = prompt("How old are you?");

while (isNaN(userAge) || userAge === null || userAge === "") {
  userAge = prompt("How old are you?", [userAge]);
}

let userAnswer;

if (userAge < 18) {
  alert(`You are not allowed to visit this website`);
} else if (userAge >= 18 && userAge <= 22) {
  userAnswer = confirm(`Are you sure you want to continue?`);
  if (userAnswer === true) {
    alert(`Welcome, ` + userName);
  } else {
    alert(`You are not allowed to visit this website`);
  }
} else {
  alert(`Welcome, ` + userName);
}
